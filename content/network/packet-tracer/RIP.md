---
title: "Konfigurasi RIPv1 dan RIPv2 di Packet Tracer (Dynamic Routing) - Packet Tracer"
date: 2022-03-09T19:37:45+07:00
draft: false
---

# Konfigurasi RIPv1 dan RIPv2 di Packet Tracer (Dynamic Routing)

## Apa itu dynamic routing?

---

Dynamic Routing atau Routing Dinamis adalah teknik routing yang mampu membuat tabel routing secara otomatis dan real-time berdasarkan lalu lintas jaringan dan beberapa router yang saling terhubung. Routing Dinamis mengijinkan router untuk saling berbagi informasi network, dan mengijinkan router-router tersebut untuk memilih jalur terbaik ke tujuan/destination.

Jenis jenis protokol routing dinamis:

1. RIP	->	Routing Information Protokol
2. OSPF	->	Open Shortest Path First
3. IGRP	->	Internal Gateway Routing Protokol
4. EIGRP ->	Enhanced Interior Gateway Routing Protocol 
5. BGP	->	Border Gateway Protokol
6. IS-IS -> Intermediate System to Intermediate System 

## Pengertian RIP (Routing Information Protokol)

---

Routing Information Protocol (RIP) adalah salah satu Routing Protocol yang menggunakan Distance Vector, oleh karena itu RIP menggunakan jumlah Hop untuk menentukan cara terbaik ke sebuah alamat jaringan tertentu, tetapi RIP secara default memiliki jumlah hop maksimum yaitu 15 Hop. Oleh karena itu, Hop ke-16 dan seterusnya akan dianggap tidak terjangkau (Unreachable). Oleh karena itu juga, RIP dapat bekerja dengan baik di jenis jaringan yang kecil, tetapi RIP tidak efisien pada network yang besar atau pada jaringan yang memiliki jumlah Router yang banyak.

RIP untuk IPv4 dibagi menjadi 2 versi, yaitu RIPv1 & RIPv2. Sedangkan untuk IPv6 dapat menggunakan RIPng. RIPv1 mengirimkan Routing Table secara lengkap ke semua interface yang aktif setiap 30 detik. RIPv1 menggunakan Classful Routing, yang artinya RIPv1 tidak mendukung Subnetting. Sedangkan RIPv2 sudah menyediakan sesuatu yang disebut dengan Prefix Routing, yang berisi informasi SubnetMask.



## Konfigurasi RIP di Packet Tracer

---

Topology

![topology](/notes/image/RIP1.png)

### Konfigurasi IP Address device

---

#### Konfigurasi IP Address di router 0

- Interface fa0/0 = 192.168.10.1/24
- Interface se2/0 = 10.10.10.1/24

<p/>

	Router>enable 
	Router#configure terminal 
	Router(config)#interface fa0/0
	Router(config-if)#ip address 192.168.10.1 255.255.255.0
	Router(config-if)#no sh
	Router(config-if)#inter se2/0
	Router(config-if)#ip address 10.10.10.1 255.255.255.0
	Router(config-if)#no sh
	Router(config-if)#exit
	Router(config)#

#### DHCP server di router 0
	
	Router(config)#ip dhcp pool dhcp10
	Router(dhcp-config)#network 192.168.10.0 255.255.255.0
	Router(dhcp-config)#default-router 192.168.10.1
	Router(dhcp-config)#exit
	Router(config)#ip dhcp excluded-address 192.168.10.1
	Router(config)#

Aktifkan semua dhcp client pada semua client yang berada di network router 0 
	
#### Konfigurasi IP Address di router 1

- Interface se2/0 = 10.10.10.2/24
- Interface fa0/0 = 192.168.20.1/24
- Interface fa1/0 = 192.168.30.1/24
- interface se3/0 = 10.10.20.1/24

<p/>

	Router#conf t
	Router(config)#inter se2/0
	Router(config-if)#ip add 10.10.10.2 255.255.255.0
	Router(config-if)#no sh
	Router(config-if)#inter fa0/0
	Router(config-if)#ip addr 192.168.20.1 255.255.255.0
	Router(config-if)#no sh
	
	Router(config-if)#inter fa1/0
	Router(config-if)#ip add 192.168.30.1 255.255.255.0
	Router(config-if)#no sh
	
	Router(config-if)#inter se3/0
	Router(config-if)#ip address 10.10.20.1 255.255.255.0
	Router(config-if)#no sh

#### DHCP server di router 1

	Router(config)#ip dhcp pool dhcp20
	Router(dhcp-config)#network 192.168.20.0 255.255.255.0
	Router(dhcp-config)#default-router 192.168.20.1
	Router(dhcp-config)#exit
	Router(config)#
	Router(config)#ip dhcp pool dhcp30
	Router(dhcp-config)#network 192.168.30.0 255.255.255.0
	Router(dhcp-config)#default-router 192.168.30.1
	Router(dhcp-config)#exit
	Router(config)#ip dhcp excluded-address 192.168.30.1
	Router(config)#ip dhcp excluded-address 192.168.20.1
	Router(config)#

Aktifkan semua dhcp client pada semua client yang berada di network router 1

#### Konfigurasi IP Address di router 2

- Interface se2/0 = 10.10.20.2/24
- interface fa0/0.40 = 192.168.40.1
- interface fa0/0.50 = 192.168.50.1
	
<p/>

	Router>enable 
	Router#conf ter
	Router(config)#inter se2/0
	Router(config-if)#ip add
	Router(config-if)#ip address 10.10.20.2 255.255.255.0
	Router(config-if)#no sh
	
	Router(config-if)#inter fa0/0
	Router(config-if)#no sh
	Router(config-if)#
	Router(config-if)#inter fa0/0.40
	Router(config-subif)#encapsulation dot1Q 40
	Router(config-subif)#ip address 192.168.40.1 255.255.255.0
	Router(config-subif)#inter fa0/0.50
	Router(config-subif)#encapsulation dot1Q 50
	Router(config-subif)#ip address 192.168.50.1 255.255.255.0

#### DHCP server di router 2

	Router(config)#ip dhcp pool dhcp40
	Router(dhcp-config)#network 192.168.40.0 255.255.255.0
	Router(dhcp-config)#default-router 192.168.40.1
	Router(dhcp-config)#ip dhcp pool dhcp50
	Router(dhcp-config)#network 192.168.50.0 255.255.255.0
	Router(dhcp-config)#default-router 192.168.50.1
	Router(dhcp-config)#
	Router(dhcp-config)#exit
	Router(config)#ip dhcp excluded-address 192.168.40.1
	Router(config)#ip dhcp excluded-address 192.168.50.1
	Router(config)#
	
Aktifkan semua dhcp client pada semua client yang berada di network router 2
	
#### Konfigurasi vlan switch 1

	Switch>enable 
	Switch#configure terminal 
	Switch(config)#vlan 40
	Switch(config-vlan)#name kelasA
	Switch(config-vlan)#vlan 50
	Switch(config-vlan)#name kelasB
	Switch(config-vlan)#inter fa0/2
	Switch(config-if)#switchport access vlan 40
	Switch(config-if)#interface fa0/3
	Switch(config-if)#switchport access vlan 50
	Switch(config-if)#inter fa0/1
	Switch(config-if)#switchport mode trunk 

### Mengaktifkan RIPv1

Untuk mengaktifkan RIPv1, masukkan perintah "router rip", diikuti perintah "network \<network address\>", masukkan semua network address yang ada di router tersebut.

- Router 0
<p/>

	Router(config)#router rip
	Router(config-router)#network 192.168.10.0
	Router(config-router)#network 10.10.10.0
	Router(config-router)#

- Router 1
<p/>

	Router(config)#router rip
	Router(config-router)#network 10.10.10.0
	Router(config-router)#network 192.168.20.0
	Router(config-router)#network 192.168.30.0
	Router(config-router)#network 10.10.20.0
	Router(config-router)#

- Router 2
<p/>

	Router(config)#router rip
	Router(config-router)#network 10.10.20.0
	Router(config-router)#network 192.168.40.0
	Router(config-router)#network 192.168.50.0
	Router(config-router)#

#### Pengujian

---

Hasilnya, semua device akan saling terhubung jika kita lakukan ping atau kirim pesan.

![hasil](/notes/image/RIP1-hasil.png)

### Mengaktifkan RIPv2

---

Tambahkan perintah "version 2" setelah menjalankan perintah "router rip" untuk mengaktifkan RIPv2

- Router 0
<p/>

	Router(config)#router rip
	Router(config-router)#version 2
	Router(config-router)#network 192.168.10.0
	Router(config-router)#network 10.10.10.0
	Router(config-router)#

- Router 1
<p/>

	Router(config)#router rip
	Router(config-router)#version 2
	Router(config-router)#network 10.10.10.0
	Router(config-router)#network 192.168.20.0
	Router(config-router)#network 192.168.30.0
	Router(config-router)#network 10.10.20.0
	Router(config-router)#

- Router 2
<p/>

	Router(config)#router rip
	Router(config-router)#version 2
	Router(config-router)#network 10.10.20.0
	Router(config-router)#network 192.168.40.0
	Router(config-router)#network 192.168.50.0
	Router(config-router)#
