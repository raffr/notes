---
title: "Konfigurasi Ether Channel (LACP) layer 2 di Switch - Packet Tracer"
date: 2023-01-19T19:17:44+07:00
draft: false
---

# EtherChannel (LACP) Layer 2 dan VLan Menggunakan Switch - Packet Tracer


Penjelasan sederhana dari etherchannel adalah menggabungkan dua buah physical interface atau lebih ke dalam satu logical interface. Gabungan dari beberapa port bisa disebut sebagai channel group dan akan membuat interface baru yang bernama port-channel

Protocol dari etherchanel ada tiga, diantaranya :

**PAGP (Port Aggregation Control Protocol)**

 Protocol etherchannel proprietary Cisco. Mode :

 a. Desirable : Mengajak untuk menjadi etherchannel PAGP.

 b. Auto : Menunggu untuk dijadikan etherchannel PAGP

**LACP (Link Aggregation Control Protocol)**

 Protocol etherchannel sama seperti PAGP, hanya saja tidak

 proprietary Cisco Mode :
 Active :
  - Mengajak untuk menjadi etherchannel LACP
  - Passive :Menunggu untuk dijadikan etherchannel LACP.
  - Etherchannel layer 3

**Protocol etherchannel yang menggunakan MLS (MultiLayer Switch).**

 Mode :
  ON : Mengajak menjadi etherchannel layer 3.


![topology](/notes/image/topology-lacp.png)

Pada percobaan kali ini, semua pc sudah di setting ip secara statis

## Konfigurasi MultiLayer Switch

---

Setting vlan name dan vlan interface

	Switch>
	Switch>enable
	Switch#configure terminal
	Enter configuration commands, one per line.  End with CNTL/Z.
	Switch(config)#vlan 10
	Switch(config-vlan)#name ruang1
	Switch(config-vlan)#vlan 20
	Switch(config-vlan)#name ruang2
	Switch(config-vlan)#exit
	Switch(config)#
	Switch(config)#interface vlan 10
	Switch(config-if)#ip address 192.168.10.1 255.255.255.248
	Switch(config-if)#
	Switch(config-if)#interface vlan 20
	Switch(config-if)#ip address 192.168.20.1 255.255.255.248
	Switch(config-if)#exit

Hidupkan routing di multilayer switch

	Switch(config)#ip routing
	Switch(config)#

Setting Channel Group pada masing masing interface

Contoh:

- fa0/1 dan fa0/2 akan di set ke channel group 1 mode active
- Sedangkan fa0/3 dan fa0/4 akan di set ke channel group 2 mode active

<p>

	Switch(config)#
	Switch(config)#interface range fa0/1-2
	Switch(config-if-range)#channel-group 1 mode active
	Switch(config-if-range)#
	Switch(config-if-range)#interface range fa0/3-4
	Switch(config-if-range)#channel-group 2 mode active
	Switch(config-if-range)#

cara check status port-channel dengan menggunakan perintah **show interface portchannel \<id\>**

	Switch#show interfaces port-channel 1
	Port-channel1 is down, line protocol is down (disabled)
	  Hardware is EtherChannel, address is 00e0.f745.8399 (bia 00e0.f745.8399)
	  MTU 1500 bytes, BW 300000 Kbit, DLY 1000 usec,
	     reliability 255/255, txload 1/255, rxload 1/255
	  Encapsulation ARPA, loopback not set
	  Keepalive set (10 sec)
	  Half-duplex, 300Mb/s
	  input flow-control is off, output flow-control is off
	  Members in this channel: Fa0/1 ,Fa0/2 ,
	  ARP type: ARPA, ARP Timeout 04:00:00
	  Last input 00:00:08, output 00:00:05, output hang never
	  Last clearing of "show interface" counters never
	  Input queue: 0/75/0/0 (size/max/drops/flushes); Total output drops: 0
	  Queueing strategy: fifo
	  Output queue :0/40 (size/max)
	  5 minute input rate 0 bits/sec, 0 packets/sec
	  5 minute output rate 0 bits/sec, 0 packets/sec
	     956 packets input, 193351 bytes, 0 no buffer
	     Received 956 broadcasts, 0 runts, 0 giants, 0 throttles
	     0 input errors, 0 CRC, 0 frame, 0 overrun, 0 ignored, 0 abort
	     0 watchdog, 0 multicast, 0 pause input
	     0 input packets with dribble condition detected
	 --More--

Tambahkan mode trunk pada masing masing port channel interface

	Switch(config)#
	Switch(config)#interface port-channel 1
	Switch(config-if)#switchport trunk encapsulation dot1q
	Switch(config-if)#
	Switch(config-if)#interface port-channel 2
	Switch(config-if)#switchport trunk encapsulation dot1q
	Switch(config-if)#


## Konfigurasi Switch 0 dan Switch 1

---

Konfigurasi vlan pada masing masing switch

Switch 0

	Switch>
	Switch>enable
	Switch#configure terminal
	Enter configuration commands, one per line.  End with CNTL/Z.
	Switch(config)#vlan 10
	Switch(config-vlan)#name ruang1
	Switch(config-vlan)#vlan 20
	Switch(config-vlan)#name ruang2
	Switch(config-vlan)#exit
	Switch(config)#interface fa0/3
	Switch(config-if)#switchport access vlan 10
	Switch(config-if)#interface fa0/4
	Switch(config-if)#switchport access vlan 20
	Switch(config-if)#exit
	Switch(config)#

Switch 1

	Switch>
	Switch>enable
	Switch#configure terminal
	Enter configuration commands, one per line.  End with CNTL/Z.
	Switch(config)#vlan 10
	Switch(config-vlan)#name ruang1
	Switch(config-vlan)#vlan 20
	Switch(config-vlan)#name ruang2
	Switch(config-vlan)#exit
	Switch(config)#interface fa0/3
	Switch(config-if)#switchport access vlan 10
	Switch(config-if)#interface fa0/4
	Switch(config-if)#switchport access vlan 20
	Switch(config-if)#exit
	Switch(config)#

Konfigurasi channel group pada masing masing switch

switch 0

	Switch(config)#
	Switch(config)#interface range fa0/1-2
	Switch(config-if-range)#channel-group 1 mode active
	Switch(config-if-range)#

switch 1

	Switch(config)#interface range fa0/1-2
	Switch(config-if-range)#channel-group 1 mode active
	Switch(config-if-range)#

Set trunk mode pada masing masing port-channel

switch 0

	Switch(config)#interface port-channel 1
	Switch(config-if)#switchport mode trunk

switch 1


	Switch(config)#interface port-channel 1
	Switch(config-if)#switchport mode trunk

Setelah semua telah selesai, Lakukan ping antar client pada jaringan tersebut, pastikan semua client dapat terkoneksi dengan baik
