---
title: "Port Security - Packet Tracer"
date: 2023-02-01T21:23:53+07:00
draft: false
---

# Port Security - Packet Tracer

## Apa itu Port Security?

---

Port security adalah sebuah keamanan yang terdapat pada perangkat switch cisco. Dengan menggunakan fitur port security kita bisa membatasi jumlah host yang dapat terhubung dengan switch dan kita juga bisa menentukan device apa saja yang boleh terhubung dengan switch dengan membaca mac address dari device tersebut.

Pengaturan port security dibagi menjadi 3 jenis:

1. static

 Menambahkan daftar mac address yang dapat terhubung ke switch secara manual, dan ketika switch mati daftar mac address masih tersimpan di switch tersebut.

2. dynamic

 Switch akan otomatis menambahkan mac address sesuai dengan device yang pertama kali terhubung ke interface switch. Tetapi data dari mac address tersebut akan hilang jika switch mati.

3. sticky

 Switch akan otomatis menambahkan mac address sesuai dengan device yang pertama kali terhubung ke interface switch. Jika switch mati maka data tidak akan hilang.

Beberapa mode **violation** :

1. Protect

 Akan membuang (drop) packet yang dikirim oleh host yang tidak diijinkan. Jika host melakukan perintah ping, maka akan menghasilkan output **request timed out**

2. Restrict

 Sama seperti mode protect, pada mode restrict packet yang dikirim oleh host juga akan di drop oleh switch. Tetapi, pada mode ini interface akan menghitung jumlah violation yang dilakukan oleh host. Untuk menampilkan jumlah violation yang telah dilakukan oleh host dapat menggunakan perintah **_show port-security interface [nama interface]_**

3. Shutdown

 Pada mode shutdown, jika terdapat violation dilakukan oleh host, maka interface yang terhubung dengan host tersebut akan ter shutdown atau mati.

## Contoh Konfigurasi Port Security

---

### Topology

![topology](/notes/image/topology-port-security.png)

Pastikan semua pc telah di setting ip static sesuai topology

### Konfigurasi Port Security di Switch 0

Setting port security pada interface **fa0/1**

	Switch>
	Switch>enable
	Switch#configure terminal
	Switch(config)#interface fa0/1
	Switch(config-if)#switchport mode access
	Switch(config-if)#switchport port-security
	Switch(config-if)#switchport port-security maximum 3
	Switch(config-if)#switchport port-security violation shutdown

Cek port-security hidup atau tidak, dan juga cek apakah mac address yang telah terdaftar telah sesuai

	Switch(config-if)#do show port-security interface fa0/1
	Port Security              : Enabled
	Port Status                : Secure-up
	Violation Mode             : Shutdown
	Aging Time                 : 0 mins
	Aging Type                 : Absolute
	SecureStatic Address Aging : Disabled
	Maximum MAC Addresses      : 3
	Total MAC Addresses        : 1
	Configured MAC Addresses   : 0
	Sticky MAC Addresses       : 0
	Last Source Address:Vlan   : 000D.BDA7.5901:1
	Security Violation Count   : 0

	Switch(config-if)#do show mac address-table int fa0/1
	          Mac Address Table
	-------------------------------------------

	Vlan    Mac Address       Type        Ports
	----    -----------       --------    -----

	   1    000d.bda7.5901    STATIC      Fa0/1
	Switch(config-if)#

Jika mac address dari host belum terdaftar semua, lakukan ping antar client sampai mac address terdaftar semua di switch dan cek lagi dengan perintah _do show mac address-table interface fa0/1_

	Switch(config-if)#do show mac address-table int fa0/1
	          Mac Address Table
	-------------------------------------------

	Vlan    Mac Address       Type        Ports
	----    -----------       --------    -----

	   1    000d.bda7.5901    STATIC      Fa0/1
	   1    0060.7005.680b    STATIC      Fa0/1
	   1    00d0.d3ca.49b0    STATIC      Fa0/1
	Switch(config-if)#

Setting port security di interface **fa0/2**

	Switch(config-if)#interface fa0/2
	Switch(config-if)#switchport mode access
	Switch(config-if)#switchport port-security
	Switch(config-if)#switchport port-security mac-address 0002.1720.194A
	Switch(config-if)#switchport port-security violation protect
	Switch(config-if)#

Cek port security

	Switch(config-if)#do sh port-security interface fa0/2
	Port Security              : Enabled
	Port Status                : Secure-up
	Violation Mode             : Protect
	Aging Time                 : 0 mins
	Aging Type                 : Absolute
	SecureStatic Address Aging : Disabled
	Maximum MAC Addresses      : 1
	Total MAC Addresses        : 1
	Configured MAC Addresses   : 1
	Sticky MAC Addresses       : 0
	Last Source Address:Vlan   : 0002.1720.194A:1
	Security Violation Count   : 0
	Switch(config-if)#do sh mac address-table int fa0/2
	          Mac Address Table
	-------------------------------------------

	Vlan    Mac Address       Type        Ports
	----    -----------       --------    -----

	   1    0002.1720.194a    STATIC      Fa0/2
	Switch(config-if)#

Setting port security interface **fa0/3**

	Switch(config-if)#interface fa0/3
	Switch(config-if)#switchport mode access
	Switch(config-if)#switchport port-security
	Switch(config-if)#switchport port-security mac-address sticky
	Switch(config-if)#switchport port-security violation restrict
	Switch(config-if)#exit

Lakukan ping dari host client dan pastikan mac address telah terdaftar

	Switch(config-if)#do sh mac address-table interface fa0/3
	          Mac Address Table
	-------------------------------------------

	Vlan    Mac Address       Type        Ports
	----    -----------       --------    -----

	   1    0001.c930.e09b    STATIC      Fa0/3
	Switch(config-if)#

### Testing

Percobaan pertama, tambahkan satu pc di switch1 yang terhubung dengan interface switch 0, lalu lakukan ping pada pc tambahan tersebut pada salah satu host lain.

![tambahan pertama](/notes/image/pc-tambahan-port-security-1.png)

![tambahan ping](/notes/image/pc-tambahan-port-security-2.png)

Maka interface fa0/1 pada switch 0 akan otomatis mati karena PC TAMBAHAN yang tidak memiliki hak akses akan mentrigger port security pada interface fa0/1 yang mana violation nya di set ke shutdown.

Untuk menghidupkan kembali interface yang telah mati bisa mengetikkan perintah *shutdown* lalu *no shutdown* pada interface fa0/1 di switch0

Percobaan kedua, ganti pc yang terhubung dengan fa 0/2 (yaitu pc3) dengan PC TAMBAHAN dan lakukan ping dari PC TAMBAHAN ke host lain.

![ganti pc pertama](/notes/image/ganti-pc-port-security-1.png)

![testing ganti pc pertama](/notes/image/testing-ganti-pc-port-security-1.png)

Pada percobaan tersebut output yang ditampilkan dari perintah ping akan menampilkan request timed out.

Percobaan ketiga, ganti pc yang terhuung dengan fa0/3 dengan (yaitu pc2) dengan PC TAMBAHAN dan lakukan ping di PC TAMBAHAN ke host lain.

![ganti pc kedua](/notes/image/ganti-pc-port-security-2.png)

![](/notes/image/testing-ganti-pc-port-security-2.png)

Output yang dikeluarkan pada perintah ping juga menampilkan Request timed out yang artinya PC TAMBAHAN tidak bisa melakukan komunikasi dengan pc lain.
