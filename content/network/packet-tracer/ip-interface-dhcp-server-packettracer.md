---
title: "Mengaktifkan fungsi routing di Switch layer 3 - Packet Tracer"
date: 2022-03-09T19:37:45+07:00
draft: false
---

# Mengaktifkan fungsi routing di Switch layer 3 - Packet Tracer

## Menambahkan IP Address ke Switch Interface

---

Menambahkan IP Address di switch hanya bisa di konfigurasi di L3 switch, contoh beberapa seri switch antara lain, seperti seri 3560 dan 3650.


![Topology](/notes/image/ss_switchIP.png)

Fungsi dari perintah "ip routing" berfungsi untuk menaktifkan fungsi-fungsi router ke switch. Perintah "no switchport" berguna agar port pada switch bisa diberikan ip address.

	Switch>enable 
	Switch#configure terminal 
	Switch(config)#interface fa0/1
	Switch(config)#ip routing
	Switch(config-if)#no switchport 
	Switch(config-if)#ip address 192.168.10.1 255.255.255.0
	Switch(config-if)#interface fa0/2
	Switch(config-if)#no switchport 
	Switch(config-if)#ip address 192.168.20.1 255.255.255.0

## DHCP Server

---

Konfigurasi DHCP server pada switch konfigurasinya mirip dengan DHCP server pada router

	# Menambahkan pool dhcp
	Switch(config)#ip dhcp pool pool10
	Switch(dhcp-config)#network 192.168.10.0 255.255.255.0
	Switch(dhcp-config)#default-router 192.168.10.1
	Switch(dhcp-config)#exit
	Switch(config)#ip dhcp pool pool20
	Switch(dhcp-config)#network 192.168.20.0 255.255.255.0
	Switch(dhcp-config)#default-router 192.168.20.1
	Switch(dhcp-config)#exit
	# IP Address pengecualian
	Switch(config)#ip dhcp excluded-address 192.168.10.1
	Switch(config)#ip dhcp excluded-address 192.168.20.1
	Switch(config)#
