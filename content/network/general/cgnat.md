# CGNAT

IPv4 merupakan protokol internet yang mendominasi internet saat ini, dengan jumlah totalnya mencapai 4,294,967,296. Angka tersebut terlihat sangat banyak ketika IPv4 diciptakan di tahun 1980. Tapi internet telah berkembang dengan sangat pesat sejak saat itu, semakin banyaknya pengguna internet, maka akan semakin berkurangnya jumlah IPv4 address yang tersedia, hingga _IPv4 address exhaustion_ pun terjadi.

Carrier-Grade NAT memungkinkan ISP untuk menyediakan akses internet ke sejumlah besar pelanggan menggunakan public ip address yang terbatas. CGNAT merupakan salah satu solusi sementara dari masalah IPv4 exhaustion, dan diperkirakan akan sepenuhnya dihapus ketika IPv6 telah diadopsi secara luas.

CGNAT memiliki manfaat yang lumayan besar dalam ISP seperti penghematan biaya pada public ipv4 address serta perlindungan terhadap ip pools mereka. Tetapi, CGNAT memiliki beberapa kekurangan yang harus diperhatikan.

Disamping perkembangannya yang cepat, sebenarnya sudah ada beberapa initiatif untuk menyelamatkan habisnya IPv4 address, seperti CIDR, Private addressing, NAT, IPv6, dll.

Meskipun inisiatif inisiatif tersebut sudah dilakukan, IANA (organisasi yang mengatur ipv4 addressing) telah kehabisan IPv4 address. jadi hanya RIR (Regional Internet Registries) saja yang dapat mengalokasikan alamat ip baru. Ditambah lagi negara negara seperti China dan India juga telah dihadapkan dengan kekurangan IPv4 address.

untuk menghadapi masalah ini, satu satunya solusi jangka panjangnya yaitu menggunakan IPv6. Walaupun IPv6 telah digunakan di beberapa negara, adopsi skala penuhnya masih memerlukan waktu yang sangat lama.

Sebelum beralih sepenuhnya ke IPv6, para operator telekomunikasi dan service provider mulai menggunakan double-NAT pada sekala yang lebih besar di seluruh jaringan mereka. Solusi tersebut dikenal dengan sebutan CGNAT

## What is CGNAT

Carrier-Grade NAT, atau yang dikenal biasanya dengan sebutan CGN atau CGNAT atau Large Scale NAT (LSN) telah mulai digunakan secara masiv sekitar tahun 2014 oleh operator jaringan mobile. Dikarenakan operator-operator tersebut mulai merasakan bertambahnya permintaan dari IPv4 address in jaringan mobile mereka, oleh karena itu mereka sesegera mungkin menggunakan teknologi jaringan CGNAT.

Walaupun banyak ISPs tetap memberikan public IP address, semakin lama akan semakin umum terjadi bahwa ip public address yang kita dapatkan sebenarnya berjalan melewati CGNAT.

Seperti halnya NAT yang biasa digunakan di dalam LAN, CGNAT melakukan hal yang sama tapi selangkah lebih jauh, berjalan di dalam jaringan internet. dengan menggunakan NAT biasa, satu public ip address akan di bagikan ke dalam banyak perangkat di belakang router kita. Tapi, dengan menggunakan CGNAT, satu public ip address akan dibagikan diantaranya ke berbagai router pelanggan. IP Address yang didapatkan oleh router kita yaitu diambil dari ketersediaan alokasi dari CGNAT ip range. ketika data kita ingin mencapai jaringan internet, maka CGNAT akan mentranslasikan CGNAT ip private address ke dalam CGNAT ip public address. ratusan pelanggan yang terkoneksi di CGNAT tersebut akan berbagi ip public address yang sama. CGNAT dari ISP akan terus melacak setiap koneksi sehingga ketika data yang diterima mendapatkan pesan balasan, CGNAT dapat menentukan pelanggan mana yang akan menerima data balasan tersebut.

Karena CGNAT banyak digunakan di jaringan seluler mobile device, maka kita kemungkinan besar sudah menggunakan jaringan CGNAT. Jika koneksi internet yang ada di hp kita berjalan dengan lancar tanpa ada kendala, bisa kita lihat CGNAT dapat berjalan baik di banyak aplikasi sehari hari kita.

Jika sebuah ISP menggunakan standar RFC 1918 pada ip address CGNAT mereka, maka akan memungkinkan terjadinya address collision dan routing failures ketika pelanggan yang terhubung ke CGNAT tersebut juga menggunakan range ip RFC 1918. Karena permasalahan itulah yang membuat IETF menerbitkan RFC 6598 yang menjelaskan tentang reservasi alamat ip yang digunakan untuk CGNAT, blok alamat yang dialokasikan adalah 100.64.0.0/10 yaitu alamat dari 100.64.0.0 hingga 100.127.255.255.

## How does CGNAT Work?

Sama halnya seperti NAT tradisional, CGNAT bekerja dengan translasikan private ip address ke dalam public ip address (dan sebaliknya). Perbedaan utama antara NAT tradisional dengan CGNAT adalah CGNAT melakukan double-NAT untuk jumlah perangkat yang berskala lebih luas.

![cgnat-img](/notes/image/CGNAT_02.png)

**1. Customer Private Network**

Customer 1, customer 2, dan customer 3 menggunakan private IPv4 address masing - masing (RFC1918). Setiap customer memiliki Customer Premises Equipment (CPE) masing-masing, yang mana akan melakukan NAT pertama dilakukan. Translasi dilakukan dengan mengubah private IPv4 address (RFC1918) ke private IPv4 address (RFC6598) yang diberikan oleh router CGNAT. Di dalam jaringan private tersebut, setiap packet yang dikirimkan oleh client akan ditranslasikan IPv4 address nya kemudian ditambahkan sebuah angka port unik atau yang dikenal sebagai port assignment. hal ini dilakukan untuk membantu membedakan dan memanage setiap packet dari client. 

**2. Provider's Private Network: CGNAT in action**

CPE akan men-forward traffic yang keluar ke jaringan ISP, dimana CGNAT berada. CGNAT akan melakukan NAT yang kedua kalinya menggunakan private IPv4 address (RFC6598) 100.64.0.0/10. CGNAT akan menggantikan private IPv4 address (RFC6598) ke IPv4 address public. Tambahan, untuk memastikan beberapa clients/users yang saling berbagi IPv4 public, agar setiap koneksi dapat diidentifikasi dan di-route dengan tepat, CGNAT akan memberikan port assignment pada setiap packet dari CPE.

**3. Public IP Address**

Jaringan private dari provider (RFC6598 atau 100.64.0.0/10) dilakukan translasi ke  public IPv4 Address 203.0.11.31. Dengan cara ini berbagai pelanggan akan berbagi satu IPv4 public address yang terbatas.

## Difference between NAT and CGNAT

**Scale**

NAT biasanya digunakan dalam jaringan berskala kecil seperti di router rumah yang memungkinkan beberapa perangkat di rumah untuk berbagi satu alamat IP publik, sedangkan CGNAT biasanya digunakan di jaringan berskala besar  seperti jaringan ISP untuk memungkinkan beberapa pelanggan untuk berbagi satu IP public.

**Complexity**

NAT relatif simpel untuk implementasinya, sedangkan CGNAT jauh lebih kompleks karena skala yang jauh lebih besar.

**Support for incoming connections**

NAT memungkinkan koneksi dari luar untuk masuk ke jaringan privat, biasanya dengan menggunakan port forwarding yang diimplementasikan di router NAT, sedangkat dengan CGNAT, implementasi port forwarding akan lebih susah dan akan ketersediaan portnya juga akan terbatas karena banyak pelanggan yang akan terhubung ke satu CGNAT Public IP address. Dan port forwarding di CGNAT juga tergantung pada kebijakan dari ISP tersebut. Beberapa ISP mungkin menyediakan layanan port forwarding pada lingkup CGNAT, sementarayang lain mungkin tidak.

****


## Benefit of CGNAT

CGNAT muncul sebagai solusi dari habisnya IPv4, walaupun memberikan manfaat yang besar untuk para operator telekomunikasi, CGNAT bukan merupakan solusi yang dapat bertahan jangka panjang.

**1. Fix IPv4 Exhaustion Problems**

Seperti yang sudah kita bahas diatas, CGNAT dapat mengatasi masalah kehabisan IPv4 address yang terjadi saat ini. Operator telekomunikasi dapat menghemat penggunaan public IPv4 address yang diberikan kepada pelanggan mereka, sehingga para operator telekomunikasi dapat meningkatkan skalabilitas pelanggan mereka.

**2. Improving Security and Privacy**

Seperti halnya NAT, CGNAT memberikan lapisan keamanan tambahan dalam koneksi internet kita. Dengan CGNAT para pelanggan dapat terhindar dari potensi serangan yang bisa menargetkan spesifik public IPv4 address. Selain itu, karena para user/pelanggan tidak lagi memakai IP publik masing masing, mungkin ada ratusan atau ribuan pengguna yang berbagi alamat IP publik yang sama sehingga menyamarkan identitas pengguna.

**3. Eases Network Management**

CGNAT menyederhanakan manajemen jaringan dikarenakan satu CGNAT dapat menangani translasi IP dari para beberapa pelanggan. Dengan begitu, CGNAT mempusatkan manajemen address dari setiap pelangganya, sehingga mengurangi kompleksitas dari me-manage masing masing IP publik dari setiap pelanggannya.

## Kerugian CGNAT

**1. No longer get Public Address** 

Setiap pelanggan yang terhubung ke CGNAT tidak lagi mendapatkan IP publik dari ISP CGNAT. Artinya ketika kita ingin self-hosting suatu service, contohnya seperti setup remote desktop di PC agar bisa diakses melalui internet, membuat layanan mailserver, site-to-site VPN, kita sudah tidak lagi melakukan hal-hal tersebut. Karena yang kita dapatkan dari ISP merupakan alamat IP private dan IP publik yang digunakan di ISP pun digunakan secara bersamaan dengan beberapa pelanggannya. Tapi beberapa ISP menyediakan fitur untuk port forwarding pada CGNAT nya sehingga kita masih memungkinkan untuk melakukan self-hosting pada jaringan kita tetapi tidak semua ISP menyediakan fitur tersebut dan biasanya ketersediaan port yang diberikan oleh ISP pun terbatas.

**2. Customer’s network security and reliability concerns.**

Shared environment yang disediakan oleh CGNAT berarti banyak user atau pelanggan akan menggunakan IP publik yang sama untuk komunikasi ke internet. Meskipun di satu sisi CGNAT menciptakan perlindungan dari penyerang untuk menargetkan specific perangkat dalam suatu jaringan, sisi negatifnya akan memperlebar penyerangan, jadi ketika penyerang menargetkan satu device di jaringan internal tersebut, penyerangangan bisa berdampak ke device lain yang masih. 

Shared IP address juga bisa mengakibatkan "reputation-damaging". Jika suatu user yang masih berbagi satu IP address yang sama tersebut melakukan suatu tindakan negatif yang mengakibatkan website tertentu melakukan pemblokiran IP, maka user lain juga akan berdampak pada pemblokiran tersebut.

**3. Challenges for devices, apps, or services that depend on direct Internet communication.**

Lagi lagi, CGNAT merusak komunikasi end-to-end untuk home-device ke internet-based services. CGNAT akan membatasi kemampuan pelanggan untuk mengakses jaringan rumah mereka dari jaringan internet. Karena CGNAT menambah lapisan intermediary baru, CGNAT akan berefek ke koneksi internet.

Dengan CGNAT pelanggan biasanya tidak bisa lagi untuk mengoperasikan port forwarding or peer-to-peer connection. Tanpa adanya port forwarding pelanggan akan mengalami kesulitan untuk membangun home-based web hosting, online gaming, layanan VoIP, IoT, dll.

**4. Latency Challenges**

Salah satu tantangan paling signifikan oleh CGNAT yaitu masalah tentang latensi. Bertambahnya layer tambahan dalam jaringan, CGNAT melakukan NAT tambahan dan merutekan trafik melewati shared public IP yang pastinya akan menambah latensi. Bertambahnya latensi ini menyebabkan merugikan sebagian user, terutama user yang sangat bergantung dengan aplikasi real-time seperti online gaming, live streams, ataupun video conferencing.

