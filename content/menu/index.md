---
headless: true
---

{{< details title="**Packet Tracer**" open=true >}}

- [Spanning Tree Protocol - Cisco]({{< relref "/network/packet-tracer/stp.md" >}})

- [Mengaktifkan Fitur Routing di Switch Layer3 - Packet Tracer]({{< relref "/network/packet-tracer/ip-interface-dhcp-server-packettracer.md" >}})

- [Konfigurasi OSPF (Dynamic Routing) - Packet Tracer]({{< relref "/network/packet-tracer/ospf.md" >}})

- [Konfigurasi RIP (Dynamic Routing) - Packet Tracer]({{< relref "/network/packet-tracer/RIP.md" >}})

- [Konfigurasi EtherChannel (LACP) layer 2 di Switch - Packet Tracer]({{< relref "/network/packet-tracer/ether-channel.md" >}})

- [Konfigurasi VTP (Virtual Trunking Protocol) di Switch - Packet Tracer]({{< relref "/network/packet-tracer/vtp.md" >}})

- [Konfigurasi VOIP (Voice Over IP) Sederhana - Packet Tracer]({{< relref "/network/packet-tracer/voip-sederhana.md" >}})

- [Konfigurasi Port Security - Packet Tracer]({{< relref "/network/packet-tracer/port-security.md" >}})

{{< /details >}}


{{< details title="**Linux**" open=true >}}

- [Konfigurasi Apache Web Server HTTP di Debian]({{< relref "/network/linux/konfigurasi-apache-webserver-debian.md" >}})

- [Konfigurasi Apache Web Server HTTPS di Debian]({{< relref "/network/linux/konfigurasi-webserver-apache-https-debian.md" >}})

- [Konfigurasi Load Balancer NGINX di Debian 11]({{< relref "/network/linux/konfigurasi-load-balancing-nginx-di-debian.md" >}})

- [Konfigurasi DNS Server Bind9 di Debian]({{< relref "/network/linux/konfigurasi-dnsserver-di-debian.md" >}})

- [Konfigurasi DHCP Server di Debian]({{< relref "/network/linux/instalasi-dhcp-debian.md" >}})

- [Konfigurasi SSH Server di Debian]({{< relref "/network/linux/instalasi-ssh-server-debian.md" >}})

- [Konfigurasi Mail Server (Postfix, Dovecot, Roundcube) di Debian 11]({{< relref "/network/linux/instalasi-ssh-server-debian.md" >}})

- [Konfigurasi IP Address Interface di Debian]({{< relref "/network/linux/konfigurasi-network-debian.md" >}})

- [Konfigurasi Debian 11 Sebagai Router/Default Gateway]({{< relref "/network/linux/konfigurasi-debian-sebagai-router.md" >}})

- [File Sharing Antara Guest OS dengan Host OS di Virt Manager]({{< relref "/network/linux/file-sharing-host-dan-guest-virt-manager.md" >}})

- [Installasi Webmin di Debian 11]({{< relref "/network/linux/instalasi-webmin.md" >}})

- [Konfigurasi proftpd (FTP dan FTPS) di Debian 11]({{< relref "/network/linux/proftpd-ftp-ftps.md" >}})

- [Port Forwarding dengan IPtables]({{< relref "/network/linux/iptables-port-forwarding.md" >}})

- [Konfigurasi OpenVPN di Debian]({{< relref "/network/linux/konfigurasi-openvpn-script-debian.md" >}})

{{< /details >}}


{{< details title="**Mikrotik**" open=true >}}

- [Setting DHCP, NAT, dan Web Proxy di Mikrotik]({{< relref "/network/mikrotik/mikrotik-web-proxy.md" >}})

{{< /details >}}
