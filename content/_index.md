---
title: About
type: docs
---

# Personal Documentation

## Tentang

Website ini merupakan sebuah website pribadi, yang  bertujuan sebagai catatan/dokumentasi tentang beberapa kegiatan yang berkaitan dengan network engineering. Website ini dibuat menggunakan Static Site Generator [Hugo](https://gohugo.io), dengan tema [book](https://themes.gohugo.io/themes/hugo-book/), dan di hosting di Gitlab Pages. Jika terdapat beberapa kesalahan pada materi atau penulisan pada website ini, kirimkan pesan [disini](https://rafimf.gitlab.io/#contact)

{{< button href="https://rafimf.gitlab.io" >}}About the Author{{< /button >}}

{{< table >}}

  {{< table-sub >}} Packet Tracer  {{< /table-sub >}}

    {{< table-content href="/notes/network/packet-tracer/ip-interface-dhcp-server-packettracer" >}} Mengaktifkan Fitur Routing di Switch Layer3 {{< /table-content >}}
    {{< table-content href="/notes/network/packet-tracer/ospf" >}} Konfigurasi OSPF (Dynamic Routing) {{< /table-content >}}
    {{< table-content href="/notes/network/packet-tracer/RIP" >}} Konfigurasi RIP (Dynamic Routing) {{< /table-content >}}
    {{< table-content href="/notes/network/packet-tracer/ether-channel" >}} Konfigurasi LACP layer2 di Switch {{< /table-content >}}
    {{< table-content href="/notes/network/packet-tracer/vtp" >}} Konfigurasi VTP (Virtual Trunking Protocol) di Switch {{< /table-content >}}
    {{< table-content href="/notes/network/packet-tracer/voip-sederhana" >}} Konfigurasi VOIP (Voice Over IP) sederhana {{< /table-content >}}
    {{< table-content href="/notes/network/packet-tracer/port-security" >}} Konfigurasi Port Security {{< /table-content >}}


  {{< table-sub >}} Linux {{< /table-sub >}}

    {{< table-content href="/notes/network/linux/konfigurasi-apache-webserver-debian" >}} Konfigurasi Apache Web Server HTTP di Debian {{< /table-content >}}
    {{< table-content href="/notes/network/linux/konfigurasi-webserver-apache-https-debian" >}} Konfigurasi Apache Web Server HTTPS di Debian {{< /table-content >}}
    {{< table-content href="/notes/network/linux/konfigurasi-load-balancing-nginx-di-debian" >}} Konfigurasi Load Balancer NGINX di Debian 11 {{< /table-content >}}
    {{< table-content href="/notes/network/linux/konfigurasi-dnsserver-di-debian" >}} Konfigurasi DNS Server Bind9 di Debian {{< /table-content >}}
    {{< table-content href="/notes/network/linux/instalasi-dhcp-debian" >}} Konfigurasi DHCP Server di Debian {{< /table-content >}}
    {{< table-content href="/notes/network/linux/instalasi-ssh-server-debian" >}} Konfigurasi SSH Server di Debian {{< /table-content >}}
    {{< table-content href="/notes/network/linux/konfigurasi-network-debian" >}} Konfigurasi IP Address Interface di Debian {{< /table-content >}}
    {{< table-content href="/notes/network/linux/konfigurasi-debian-sebagai-router" >}} Konfigurasi Debian 11 Sebagai Router/Default Gateway {{< /table-content >}}
    {{< table-content href="/notes/network/linux/file-sharing-host-dan-guest-virt-manager" >}} File Sharing Antara Guest OS dengan Host OS di Virt Manager {{< /table-content >}}
    {{< table-content href="/notes/network/linux/instalasi-webmin" >}} Installasi Webmin di Debian 11 {{< /table-content >}}
    {{< table-content href="/notes/network/linux/proftpd-ftp-ftps" >}} Konfigurasi proftpd (FTP dan FTPS) di Debian 11 {{< /table-content >}}
    {{< table-content href="/notes/network/linux/iptables-port-forwarding" >}} Port Forwarding dengan IPtables {{< /table-content >}}
    {{< table-content href="/notes/network/linux/konfigurasi-openvpn-script-debian" >}} Konfigurasi OpenVPN di Debian {{< /table-content >}}

    {{< table-content href="/notes/network/linux/postfix-dovecot-roundcube-mailserver" >}} Konfigurasi Mail Server (Postfix, Dovecot, Roundcube) di Debian 11 {{< /table-content >}}

  {{< table-sub >}} Mikrotik {{< /table-sub >}}

    {{< table-content href="/notes/network/mikrotik/mikrotik-web-proxy" >}} Setting DHCP, NAT, Web Proxy di Mikrotik {{< /table-content >}}

{{< /table >}}
